var koa = require('koa');
var etag = require('koa-etag');
var fresh = require('koa-fresh');
var logger = require('koa-logger');
var compress = require('koa-compress');
var requestId = require('koa-request-id');
var bodyParser = require('koa-bodyparser');
var responseTime = require('koa-response-time');

var app = koa();
app.use(logger());
app.use(responseTime());
app.use(requestId());
app.use(bodyParser());

app.use(function *(next) {
	this.body = {'hello': 'world'};
	console.log(this);
	yield next;
});

app.use(fresh());
app.use(etag());
app.use(compress());

app.listen(3000);